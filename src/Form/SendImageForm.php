<?php

namespace Drupal\screenshot\Form;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Drupal\media\Entity\Media;
use Drupal\screenshot\Ajax\SendImageCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * In view mode send image.
 */
class SendImageForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The Token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $token;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->fileUrlGenerator = $container->get('file_url_generator');
    $instance->fileRepository = $container->get('file.repository');
    $instance->fileSystem = $container->get('file_system');
    $instance->token = $container->get('token');
    return $instance;
  }

  /**
   * Save Image.
   */
  public function saveImage(string $selector, Request $request) {
    $encoded_image = $request->request->get('screenshot');
    $url = '';
    $fid = FALSE;
    $delta = $request->request->get("delta");
    $entity_type = $request->request->get("entity_type");
    $bundle = $request->request->get("bundle");
    $field_name = $request->request->get("field_name");
    $inputFid = "{$field_name}[$delta][fids]";
    $fieldConfig = $this->entityTypeManager->getStorage('field_config')
      ->load($entity_type . '.' . $bundle . '.' . $field_name);
    $file_directory = $request->request->get("file_directory");
    if (!empty($fieldConfig)) {
      $file_directory = $fieldConfig->getSetting('file_directory');
    }
    if (!empty($encoded_image)) {
      // Create image directory.
      $file_directory = trim($file_directory, '/');
      $destination = PlainTextOutput::renderFromHtml(
        $this->token->replace($file_directory, [])
      );
      $dir = $uri = $this->configFactory()->get('system.file')->get('default_scheme') . '://';
      if (!empty($destination)) {
        $uri .= $destination . '/';
        $path = $this->fileSystem->realpath($uri);
        if ($path) {
          $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
        }
        else {
          $extract = explode('/', str_replace('\\', '/', $destination));
          foreach ($extract as $dirName) {
            $dir .= $dirName . '/';
            $path = $this->fileSystem->realpath($dir);
            $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
          }
        }
      }
      // Convert image base64 to file.
      [$dataImage, $encoded_image] = explode(",", $encoded_image);
      [$tmp, $ext] = explode('/', str_replace([';', 'base64', ','], '', $dataImage));
      unset($tmp);
      $encoded_image = str_replace(' ', '+', $encoded_image);
      $decoded_image = base64_decode($encoded_image);
      $filename = date('ymd') . '_' . rand(1000, 9999) . '.' . $ext;
      // Saves a file to the specified destination and creates a database entry.
      $file = $this->fileRepository->writeData($decoded_image, $uri . $filename, FileExists::Replace);
      $fid = $file->id();
      $uri = $file->getFileUri();
      $url = $this->fileUrlGenerator->generate($uri)->toString();
      $uid = $this->currentUser()->id();
      $file->setOwnerId($uid);
      $file->setPermanent();
      $file->save();
      if ($entity_type == 'media') {
        $title = $this->t('Screenshot');
        $image_media = Media::create([
          'name' => $title,
          'bundle' => 'image',
          'uid' => $uid,
          'status' => TRUE,
          'field_media_image' => [
            'target_id' => $fid,
            'alt' => $title,
            'title' => $title,
          ],
        ]);
        $image_media->save();
        $inputFid = $field_name;
        $fid = $image_media->id();
      }
    }
    $response = new AjaxResponse();
    if (!empty($url)) {
      $response->addCommand(new SendImageCommand($selector, $url, $inputFid, $fid));
    }

    return $response;
  }

  /**
   * Form id.
   */
  public function getFormId() {
    return 'screenshot_sendImage_form';
  }

  /**
   * Get editable configuration name.
   */
  protected function getEditableConfigNames() {
    return [$this->getFormId() . '.setting'];
  }

}
