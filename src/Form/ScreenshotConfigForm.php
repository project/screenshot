<?php

namespace Drupal\screenshot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form firebase.
 */
class ScreenshotConfigForm extends ConfigFormBase {

  /**
   * Name of the config.
   *
   * @var string
   */
  public static string $configName = 'screenshot.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::$configName];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'screenshot_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::$configName);
    $form['screenshot_local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use local libraries'),
      '#description' => $this->t('Download to <a href="@marker">markerjs2</a>, <a href="@crop">cropperjs</a> to /libraries', [
        '@marker' => 'https://github.com/ailon/markerjs2',
        '@crop' => 'https://github.com/fengyuanchen/cropperjs',
      ]),
      '#default_value' => $config->get('screenshot_local'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::$configName)
      ->set('screenshot_local', $form_state->getValue('screenshot_local'))
      ->save();
    return parent::submitForm($form, $form_state);
  }

}
