<?php

namespace Drupal\screenshot\Plugin\Field\FieldWidget;

/* cSpell:ignore markerjs cropperjs */

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'image_image' widget.
 */
#[FieldWidget(
  id: 'screenshot',
  label: new TranslatableMarkup('Screenshot'),
  field_types: ['image'],
)]
class ScreenshotWidget extends ImageWidget {

  /**
   * Constructs a ScreenshotBlock object.
   *
   * @param string $plugin_id
   *   The id of the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   Settings.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info
   *   The element information.
   * @param \Drupal\Core\Image\ImageFactory|null $image_factory
   *   The image factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface|null $configFactory
   *   Configuration service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ElementInfoManagerInterface $element_info, ?ImageFactory $image_factory = NULL, protected ?ConfigFactoryInterface $configFactory = NULL) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $element_info, $image_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('element_info'),
      $container->get('image.factory'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'show_file_input' => FALSE,
      'crop' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['show_file_input'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show file select button'),
      '#default_value' => $this->getSetting('show_file_input'),
    ];
    $elements['crop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Crop'),
      '#description' => $this->t('Crop screenshot before take note'),
      '#default_value' => $this->getSetting('crop'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if (!empty($this->getSetting('show_file_input'))) {
      $summary[] = $this->t('Show file select button');
    }
    if (!empty($this->getSetting('crop'))) {
      $summary[] = $this->t('Crop');
    }
    return $summary;
  }

  /**
   * Overrides FileWidget::formMultipleElements().
   *
   * Special handling for draggable multiple widgets and 'add more' button.
   */
  protected function formSingleElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formSingleElement($items, $delta, $element, $form, $form_state);
    if (isset($form["#parents"][0]) && $form["#parents"][0] == 'media') {
      return $element;
    }
    $id = "screenshot-field-image-$delta";
    $url_object = Url::fromRoute('screenshot.send', ['selector' => $id], ['absolute' => TRUE]);
    $url = $url_object->toString();
    $entity = $items->getEntity();
    $bundle = $entity->bundle() ?? '';
    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $field_name = $items->getName();
    $element["#alt_field_required"] = FALSE;
    $element["#title_field_required"] = FALSE;
    $showInput = $this->getSetting('show_file_input');
    if (empty($showInput)) {
      $element['#attributes']['class'][] = 'js-hidden';
    }
    $crop = $this->getSetting('crop');
    $image_style = ImageStyle::load($element["#preview_image_style"] ?? 'thumbnail');
    $effectConfig = $image_style->getEffects()->getConfiguration();
    $height = $width = FALSE;
    foreach ($effectConfig as $effect) {
      $height = $effect['data']['height'] ?? FALSE;
      $width = $effect['data']['width'] ?? FALSE;
      if (!empty($height)) {
        break;
      }
    }
    $fid = $element["#default_value"]["target_id"] ?? NULL;
    $config = $this->configFactory->get('screenshot.settings');
    $local = $config->get('screenshot_local') ? '.local' : '';
    $element['screenshot'] = [
      '#theme' => 'screenshot',
      '#id_image' => $id,
      '#attributes' => [
        'class' => ['screen-capture', 'button', 'btn', 'btn-outline-secondary'],
        'data-mode' => 'modal',
        'data-id' => $id,
        'data-url' => $url,
        'data-entity_type' => $entity_type,
        'data-entity_id' => $entity_id,
        'data-field_name' => $field_name,
        'data-delta' => $delta,
        'data-file_input' => $showInput,
        'data-fid' => $fid,
        'data-bundle' => $bundle,
        'data-height' => $height,
        'data-width' => $width,
        'data-crop' => $crop,
        'type' => 'button',
      ],
      '#attached' => [
        'library' => [
          'screenshot/markerjs2' . $local,
          'screenshot/cropperjs' . $local,
          'screenshot/screenshot',
        ],
      ],
    ];
    return $element;
  }

}
