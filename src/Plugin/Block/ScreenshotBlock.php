<?php

namespace Drupal\screenshot\Plugin\Block;

/* cSpell:ignore markerjs cropperjs */

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a screenshot block.
 */
#[Block(
  id: "screenshot_block",
  admin_label: new TranslatableMarkup("Screenshot Block"),
  category: new TranslatableMarkup("Screenshot")
)]
class ScreenshotBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a ScreenshotBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The id of the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'screenshot_type' => 'modal',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['screenshot_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => [
        'modal' => $this->t('Modal'),
        'tab' => $this->t('Crop in new tab'),
      ],
      '#description' => $this->t('Select type to display captured images?'),
      '#default_value' => $this->configuration['screenshot_type'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['screenshot_type'] = $form_state->getValue('screenshot_type');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $id = "screenshot-block-image";
    $build = [
      '#theme' => 'screenshot',
      '#id_image' => $id,
      '#attributes' => [
        'data-id' => $id,
        'data-mode' => $config['screenshot_type'],
        'class' => ['screen-capture-block'],
      ],
      '#attached' => [
        'library' => ['screenshot/screenshot'],
        'drupalSettings' => [
          'screenshot' => [$this->getFormId() => $config],
        ],
      ],
    ];
    if (!empty($config['screenshot_type']) && $config['screenshot_type'] == 'modal') {
      $config = $this->configFactory->get('screenshot.settings');
      $local = $config->get('screenshot_local') ? '.local' : '';
      $build['#attached']['library'][] = 'screenshot/cropperjs' . $local;
      $build['#attached']['library'][] = 'screenshot/markerjs2' . $local;
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  private function getFormId() {
    return 'screenshot_block';
  }

}
