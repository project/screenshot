<?php

namespace Drupal\screenshot\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command to send an image base64 field formatter.
 */
class SendImageCommand implements CommandInterface {

  /**
   * Constructor.
   *
   * @param string $selector
   *   The ID for the image element.
   * @param string $imageSrc
   *   The url image element.
   * @param string $inputFid
   *   The input fid hidden.
   * @param int $fid
   *   File id.
   */
  public function __construct(protected string $selector, protected string $imageSrc, protected string $inputFid, protected int $fid) {
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {
    return [
      'command' => 'SendImage',
      'selector' => $this->selector,
      'image_src' => $this->imageSrc,
      'input_fid' => $this->inputFid,
      'fid' => $this->fid,
    ];
  }

}
