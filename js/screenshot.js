// cSpell:ignore markerjs croppable cropend
(function ($, Drupal, drupalSettings, once) {
  "use strict";
  Drupal.behaviors.screen_capture = {
    attach: function (context, settings) {
      $(once('screen-capture', '.screen-capture')).each(function () {
        $(this).on("click", function (e) {
          let config = $(this).data();
          if(!config.mode) {
            config.mode = $(this).closest('.screen-capture-block').data('mode');
          }
          if(!config.id) {
            config.id = $(this).closest('.screen-capture-block').data('id');
          }
          if (config.file_input == 0) {
            $(this).parent().find('input[type="file"]').hide();
          }
          captureScreen(e, config);
        });
      });
    }
  }

  function captureScreen(event, config) {
    event.preventDefault();
    if (!navigator.mediaDevices || !navigator.mediaDevices.getDisplayMedia) {
      alert(Drupal.t('Your browser does not support screen capture. Please use the latest version of Chrome or Firefox.'));
      return;
    }

    navigator.mediaDevices.getDisplayMedia({video: true})
      .then(stream => {
        // Delay the screen capture to give the sharing prompt time to disappear.
        setTimeout(() => {
          handleStream(stream, config);
        }, 500); // Adjust delay as needed.
      })
      .catch(handleError);
  }

  function handleStream(stream, config) {
    const track = stream.getVideoTracks()[0];
    let imageCapture = new ImageCapture(track);
    imageCapture.grabFrame().then((bitmap) => processFrame(bitmap, track, config)).catch(handleError);
  }

  function processFrame(bitmap, track, config) {
    const canvas = document.createElement('canvas');
    drawImageOnCanvas(bitmap, canvas);
    track.stop();
    let htmlCode = '';
    if(config.mode == 'modal'){
      const capture = document.createElement('img');
      capture.id = 'captured-image';
      capture.src = canvas.toDataURL();
      if (config.crop) {
        let screenshotDialog = {
          title: Drupal.t('Crop screenshot'),
          autoResize: true,
          dialogClass: 'screenshot-dialog',
          width: '90%',
          modal: true,
          height: $(window).height() * 0.8,
          position: {my: "center", at: "top"},
          buttons: [
            {
              text: "💾 " + Drupal.t('Save'),
              click: function () {
                sendImageToServer(config, capture);
                $(this).dialog("close");
              }
            }
          ],
          close: function (event) {
            $(this).dialog('destroy').remove();
          },
        };
        let showDialog = $(capture).dialog(screenshotDialog);
        let cropper = new Cropper(capture,  {
          autoCropArea: 0.9,
          viewMode: 3,
          modal: true,
          cropBoxMovable: false,
          cropBoxResizable: true,
          cropend: () =>{
            const canvas = cropper.getCroppedCanvas();
            capture.src = canvas.toDataURL();
            cropper.destroy();
            showDialog.dialog('destroy').remove();
            $('#'+config.id).html(capture);
            showMarkerArea(capture, config);
          }
        });
      } else {
        $('#'+config.id).html(capture);
        showMarkerArea(capture, config);
      }
    } else {
      htmlCode = getHtmlCode(canvas.toDataURL());
      openInNewTab(htmlCode, track);
    }
  }

  function showMarkerArea(capture, config) {
    const markerArea = new markerjs2.MarkerArea(capture);
    markerArea.settings.displayMode = 'popup';
    markerArea.addEventListener("render",
      (event) => (capture.src = event.dataUrl)
    );
    markerArea.show();
    var savedImage = false;
    markerArea.addEventListener("render", (event) => {
      const base64Image = capture.src;
      const filename = "screenshot.png";
      capture.remove();
      if (config.url != undefined) {
        sendImageToServer(config, capture);
        savedImage = true;
      } else {
        saveImageToFile(base64Image, filename);
      }
      $('#' + config.id).html('');
      $('.screenshot-dialog .ui-dialog-titlebar-close').click();
    });
    markerArea.addEventListener("close",
      (event) => {
        if(!savedImage) {
          sendImageToServer(config, capture);
        }
      }
    );
  }
  function sendImageToServer(config, capture) {
    config.screenshot = capture.src;
    $.ajax({
      url: config.url,
      type: 'POST',
      cache: false,
      contentType: "application/x-www-form-urlencoded;charset=UTF-8",
      data: config,
      success: function (data) {
        let capture = data[0];
        // If media refresh view list thumbnail.
        $('#views-exposed-form-media-library-widget input[type="submit"]').click();
        // Add thumbnail.
        let height = 66;
        if (config.height) {
          height = config.height;
        }
        let selector = config.drupalSelector.replace('-screenshot', '-preview');
        let thumbnail = $('img[data-drupal-selector="' + selector + '"]');
        if (!thumbnail.length) {
          thumbnail = $('<img>').attr('src', capture.image_src).attr('height', height).addClass('image-style-thumbnail');
          $('#' + capture.selector).html(thumbnail);
        } else {
          thumbnail.attr('src', capture.image_src);
        }
        // Update hidden value.
        $.each(capture.input_fid.split(',') , function (index,hideInputName) {
          let inputHidden = $('input[name="' + hideInputName + '"]');
          inputHidden.val(capture.fid);
        });
      }
    });
  }
  function saveImageToFile(dataURI, filename) {
    const link = document.createElement('a');
    link.href = dataURI;
    link.download = filename;
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    // Clean up
    document.body.removeChild(link);
  }

  function drawImageOnCanvas(bitmap, canvas) {
    canvas.width = bitmap.width;
    canvas.height = bitmap.height;
    const context = canvas.getContext('2d');
    context.drawImage(bitmap, 0, 0, bitmap.width, bitmap.height);
  }

  function getHtmlCode(image) {
    return `
      <html>
        <head>
          <script src="https://unpkg.com/markerjs2/markerjs2.js" defer></script>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.6.1/cropper.min.css" />
          <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.6.1/cropper.min.js" defer></script>
        </head>
        <body>
          <img id="captured-image" src="${image}" width="100%"/>
          <script>
            function showMarkerArea(target) {
              const markerArea = new markerjs2.MarkerArea(target);
              markerArea.addEventListener("render", (event) => {
                target.src = event.dataUrl;
              });
              markerArea.show();
              markerArea.addEventListener("render", (event) => {
                const base64Image = target.src;
                const filename = "screenshot.png";
                saveImageToFile(base64Image, filename);
              });

              markerArea.addEventListener("close", (event) => {
                window.close();
              });
            }
            function saveImageToFile(dataURI, filename) {
              const link = document.createElement('a');
              link.href = dataURI;
              link.download = filename;
              link.style.display = 'none';
              document.body.appendChild(link);
              link.click();
              // Clean up
              document.body.removeChild(link);
            }
            window.addEventListener("DOMContentLoaded", () => {
              const capturedImage = document.getElementById("captured-image");
              let cropper = new Cropper(capturedImage,  {
                autoCropArea: 0.9,
                viewMode: 3,
                modal: true,
                cropBoxMovable: false,
                cropBoxResizable: true,
                cropend: () =>{
                  const canvas = cropper.getCroppedCanvas();
                  const croppedImage = canvas.toDataURL();
                  capturedImage.src = croppedImage;
                  cropper.destroy();
                  showMarkerArea(capturedImage);
                }
              });
            });
          </script>
        </body>
      </html>
    `;
  }

  function openInNewTab(htmlCode, track) {
    const newTab = window.open();
    if (!newTab) {
      console.error("Can't open new tab");
      return;
    }
    newTab.document.open();
    newTab.document.write(htmlCode);
    newTab.document.close();
    newTab.addEventListener('beforeunload', () => {
      console.log('beforeunload event', track);
      if (track && track.stop) {
        track.stop();
      } else {
        console.error("Track doesn't exist.");
      }
    });
  }

  function handleError(err) {
    console.error(Drupal.t("Can't access screen:"), err);
    // Check if the error is due to the user canceling the screen sharing prompt.
    if (err.name === 'NotAllowedError' || err.name === 'AbortError') {
      return;
    }
    alert(Drupal.t('An error occurred while trying to capture the screen. Please try again.'));
  }

}(jQuery, Drupal, drupalSettings, once));
